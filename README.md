# Hackathon Starter Pack

## What has been done
https://pad.gwdg.de/s/hackathon-starter#

But you still need to install node, npm, nvm, Pocketbase and an Editor like https://vscodium.com/ for yourself!

## Developing

1. Fork the repo in Gitlab. It is best to create a gitlab group for your team. You can also copy the project by creating a new projekt by Importing using "Repository by URL" and `https://gitlab.gwdg.de/hackyourstudies2023/webapp-template.git` as URL.
2. Clone the repo on your PC. `git clone https://gitlab.gwdg.de/hackyourstudies2023/webapp-template.git` (replace this URL with what Gitlab shows for your forked version).
3. (optional) Start VSCodium and do the rest from within the editor. You need to open a Terminal using the menu `Terminal -> New Terminal`.
4. Switch to the repository and run `npm install` to install dependencies. These will be put in `node_modules/`
5. Run a development instance with `run dev`. It shows you a webaddress you can paste into your browser. This will automatically update whenever you make changes somewhere in the project.

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

If you want to deploy this version please ask someone from the orga team.